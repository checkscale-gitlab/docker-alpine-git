DOCKER_NS ?= guardianproject-ops/docker-alpine-git
TAGS := latest

build: $(TAGS)

$(TAGS):
	docker build --pull --no-cache -t ${DOCKER_NS}:latest ${PWD}
	docker push ${DOCKER_NS}:latest
