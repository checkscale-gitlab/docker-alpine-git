FROM docker:git
LABEL maintainer="Abel Luck <abel@guardianproject.info>"

ENV PYTHONUNBUFFERED=1
RUN set -ex; \
    apk add --no-cache --update \
      make \
      jq \
      python3 ; \
    ln -sf python3 /usr/bin/python ; \
    python3 -m ensurepip ; \
    pip3 install --no-cache --upgrade pip setuptools ; \
    pip3 install --no-cache requests python-dateutil click; \
