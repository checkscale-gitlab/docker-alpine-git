# docker-alpine-git

[![pipeline status](https://gitlab.com/guardianproject-ops/docker-alpine-git/badges/master/pipeline.svg)](https://gitlab.com/guardianproject-ops/docker-alpine-git/-/commits/master)

Docker image containing a few tools we use regularly in Gitlab CI

### Available tags:

```
latest
```

## License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/guardianproject-ops/docker-alpine-git/blob/master/LICENSE.md)

This is a free software project licensed under the GNU Affero General
Public License v3.0 (GNU AGPLv3) by [The Center for Digital
Resilience](https://digiresilience.org) and [Guardian
Project](https://guardianproject.info).

